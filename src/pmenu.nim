import 
  std/os,
  draw

type
  ListItem = object
    index:        int
    reference: string
    text:      string
type
  PMenu* = object
    case_i:    bool
    bottom:    bool
    verbose*:  bool
    prompt:    string
    font:      string
    item_list: seq[ListItem]

var pmenu*: PMenu
const NAME = "pmenu : "
proc usage() =
  echo "Help!"

proc parseArgs() =
  var item_idx = 0
  let params = commandLineParams()
  var skipnext = false
  for idx, param in params:
    if skipnext:
      skipnext = false
      continue
    case param:
      of "-i":
        pmenu.case_i = true
      of "-b":
        pmenu.bottom = true
      of "-p":
        pmenu.prompt = params[idx+1]
        skipnext = true
      of "-h":
        usage()
        return
      of "-f","-fn":
        pmenu.font = params[idx+1]
        skipnext = true
      of "-v":
        pmenu.verbose = true
      else:
        if isMainModule:
          let item = ListItem(
              index: item_idx,
              text: param
          )
          item_idx += 1
          pmenu.item_list.add(item)
        else:
          echo "Warning: List Items should not be added this way when used as a library"
  if pmenu.verbose:
    echo pmenu

proc showMenu() =
  draw.prompt = NAME
  draw.fontName = pmenu.font
  draw.start()
  discard

proc main() =
  parseArgs()
  showMenu()

when isMainModule:
  main()
