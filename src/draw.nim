import 
  x11/xlib,
  x11/xutil,
  x11/x,
  x11/xft,
  x11/xrender,
  x11/keysym

const
  WINDOW_WIDTH = 300
  WINDOW_HEIGHT = 100

var
  width, height, border: cuint
  posx, posy: cint = 100
  display: PDisplay
  screen: cint
  depth: int
  win: Window
  sizeHints: XSizeHints
  wmDeleteMessage: Atom
  running: bool
  xev: XEvent
  prompt* = "pmenu : "
  inputBox = " "
  displayString = prompt & inputBox
  fontName* = "monospace:size=10:bold"
  font: PXftFont
  xftDraw: PXftDraw
  xftColor: XftColor

proc create_window = 

  display = XOpenDisplay(nil)
  if display == nil:
    quit "Failed to open display"

  screen = XDefaultScreen(display)

  width = WINDOW_WIDTH
  height = WINDOW_HEIGHT
  font = XftFontOpenName(display, screen, fontName)
  if font == nil:
    quit "Failed to load a valid font"

  depth = XDefaultDepth(display, screen)
  var rootwin = XRootWindow(display, screen)
  win = XCreateSimpleWindow(display, rootwin, posx, posy,
                            width, height, border,
                            XWhitePixel(display, screen),
                            XBlackPixel(display, screen))
  sizeHints.flags = PSize or PMinSize or PMaxSize
  sizeHints.min_width =  width.cint
  sizeHints.max_width =  width.cint
  sizeHints.min_height = height.cint
  sizeHints.max_height = height.cint
  discard XSetStandardProperties(display, win, "Simple Window", "window",
                         0, nil, 0, addr(sizeHints))
  discard XSelectInput(display, win, ButtonPressMask or KeyPressMask or 
                                     PointerMotionMask or ExposureMask)
  discard XMapWindow(display, win)

  wmDeleteMessage = XInternAtom(display, "WM_DELETE_WINDOW", false.XBool)
  discard XSetWMProtocols(display, win, wmDeleteMessage.addr, 1)

  xftDraw = XftDrawCreate(display, win, DefaultVisual(display, 0), DefaultColormap(display, 0))
  var xrenderColor: XRenderColor
  xrenderColor.red = 65535
  xrenderColor.green = 65535
  xrenderColor.blue = 65535
  xrenderColor.alpha = 65535
  discard XftColorAllocValue(
    display,
    DefaultVisual(display, 0),
    DefaultColormap(display, 0),
    xrenderColor.addr,
    xftColor.addr
  )

  running = true

proc close_window =
  discard XDestroyWindow(display, win)
  discard XCloseDisplay(display)

proc draw_screen =
  displayString = prompt & inputBox
  XftDrawStringUtf8(xftDraw, xftColor.addr, font, 2, 12, cast[PFcChar8](displayString[0].addr),displayString.len.cint)

proc handle_event: int =
  discard XNextEvent(display, xev.addr)
  case xev.theType
  of Expose:
    draw_screen()
  of ClientMessage:
    if cast[Atom](xev.xclient.data.l[0]) == wmDeleteMessage:
      running = false
  of KeyPress:
    # Yuck
    var key = XLookupKeysym(cast[PXKeyEvent](xev.addr), 0)
    echo "Key ", key, " : ", XKeySymToString(key) , " pressed"
    if key != 0:
      if key == keysym.XK_Escape:
        running = false
        return
      elif key == XK_space:
        inputBox = inputBox & " "
      elif key == keysym.XK_BackSpace:
        inputBox = inputBox[0..len(inputBox)-2]
      elif 32 <= key and key <= 255:
        let letter = XKeySymToString(key)
        inputBox = inputBox & $letter
      draw_screen()

  of ButtonPressMask, PointerMotionMask:
    echo "Mouse event - unhandled currently"
  else:
    discard

proc start*() =
  create_window()
  while running:
    let ev = handle_event()
  close_window()


